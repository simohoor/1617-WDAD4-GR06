using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.Security;

namespace Bnbooking.API.Controllers
{
    [Route("api/[controller]")]
     public class PricesController : BaseController 
     {
        private const string FAILGETENTITIES = "Failed to get Prices from the API";
        private const string FAILGETENTITYBYID = "Failed to get Price from the API by Id: {0}";

        public PricesController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }
        public async Task<IActionResult> GetPlaces([FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.Places.ToListAsync():await ApplicationDbContext.Places.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }
        [HttpGet(Name = "Get Prices")]
        public async Task<IActionResult> GetPrices([FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.Prices.Include(p => p.ExtraCosts).ToListAsync():await ApplicationDbContext.Prices.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{PriceId:int}", Name = "GetPriceById")]
        public async Task<IActionResult> GetPriceById(Int16 PriceId, [FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.Prices.Include(p => p.ExtraCosts).FirstOrDefaultAsync(o => o.Id == PriceId):await ApplicationDbContext.Prices.FirstOrDefaultAsync(o => o.Id == PriceId);
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }
    }
}
