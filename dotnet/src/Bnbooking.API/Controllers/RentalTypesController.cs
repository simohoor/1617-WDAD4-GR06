using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.Security;

namespace Bnbooking.API.Controllers
{
    [Route("api/[controller]")]
     public class RentalTypesController : BaseController 
     {
        private const string FAILGETENTITIES = "Failed to get RentalTypes from the API";
        private const string FAILGETENTITYBYID = "Failed to get RentalTypes from the API by Id: {0}";

        public RentalTypesController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "Get RentalTypes")]
        public async Task<IActionResult> GetRentalTypes([FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.RentalTypes.ToListAsync():await ApplicationDbContext.RentalTypes.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{RentalTypeId:int}", Name = "GetRentalTypeById")]
        public async Task<IActionResult> GetRentalTypeById(Int16 RentalTypeId, [FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.RentalTypes.FirstOrDefaultAsync(o => o.Id == RentalTypeId):await ApplicationDbContext.RentalTypes.FirstOrDefaultAsync(o => o.Id == RentalTypeId);
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }
    }
}
