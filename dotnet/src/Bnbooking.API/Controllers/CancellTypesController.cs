using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.Security;

namespace Bnbooking.API.Controllers
{
    [Route("api/[controller]")]
     public class CancelTypesController : BaseController 
     {
        private const string FAILGETENTITIES = "Failed to get CancelTypes from the API";
        private const string FAILGETENTITYBYID = "Failed to get CancelTypes from the API by Id: {0}";

        public CancelTypesController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "Get CancelTypes")]
        public async Task<IActionResult> GetCancelTypes([FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.CancelTypes.ToListAsync():await ApplicationDbContext.CancelTypes.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{CancelTypeId:int}", Name = "GetCancelTypeById")]
        public async Task<IActionResult> GetCancelTypeById(Int16 CancelTypeId, [FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.CancelTypes.FirstOrDefaultAsync(o => o.Id == CancelTypeId):await ApplicationDbContext.CancelTypes.FirstOrDefaultAsync(o => o.Id == CancelTypeId);
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }
    }
}
