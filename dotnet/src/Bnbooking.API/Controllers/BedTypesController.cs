using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.Security;

namespace Bnbooking.API.Controllers
{
    [Route("api/[controller]")]
     public class BedTypesController : BaseController 
     {
        private const string FAILGETENTITIES = "Failed to get BedTypesController from the API";
        private const string FAILGETENTITYBYID = "Failed to get BedTypesController from the API by Id: {0}";

        public BedTypesController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "Get BedTypes")]
        public async Task<IActionResult> GetBedTypes([FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.BedTypes.ToListAsync():await ApplicationDbContext.BedTypes.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{BedTypeId:int}", Name = "GetBedTypeById")]
        public async Task<IActionResult> GetBedTypeById(Int16 BedTypeId, [FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.BedTypes.FirstOrDefaultAsync(o => o.Id == BedTypeId):await ApplicationDbContext.BedTypes.FirstOrDefaultAsync(o => o.Id == BedTypeId);
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }
    }
}
