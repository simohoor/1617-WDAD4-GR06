using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.Security;

namespace Bnbooking.API.Controllers
{
    [Route("api/[controller]")]
     public class ExtraCostsController : BaseController 
     {
        private const string FAILGETENTITIES = "Failed to get ExtraCosts from the API";
        private const string FAILGETENTITYBYID = "Failed to get ExtraCost from the API by Id: {0}";

        public ExtraCostsController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }
        public async Task<IActionResult> GetPlaces([FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.Places.ToListAsync():await ApplicationDbContext.Places.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }
        [HttpGet(Name = "Get ExtraCosts")]
        public async Task<IActionResult> GetExtraCosts([FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.ExtraCosts.Include(p => p.Prices).ToListAsync():await ApplicationDbContext.ExtraCosts.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{ExtraCostId:int}", Name = "GetExtraCostById")]
        public async Task<IActionResult> GetExtraCostById(Int16 ExtraCostId, [FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.ExtraCosts.Include(p => p.Prices).FirstOrDefaultAsync(o => o.Id == ExtraCostId):await ApplicationDbContext.ExtraCosts.FirstOrDefaultAsync(o => o.Id == ExtraCostId);
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }
    }
}
