using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.Security;

namespace Bnbooking.API.Controllers
{
    [Route("api/[controller]")]
     public class PlacesController : BaseController 
     {
        private const string FAILGETENTITIES = "Failed to get places from the API";
        private const string FAILGETENTITYBYID = "Failed to get place from the API by Id: {0}";

        public PlacesController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "Get Places")]
        public async Task<IActionResult> GetPlaces([FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.Places.ToListAsync():await ApplicationDbContext.Places.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{placeId:int}", Name = "GetPlaceById")]
        public async Task<IActionResult> GetPlaceById(Int16 placeId, [FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.Places.FirstOrDefaultAsync(o => o.Id == placeId):await ApplicationDbContext.Places.FirstOrDefaultAsync(o => o.Id == placeId);
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }
    }
}
