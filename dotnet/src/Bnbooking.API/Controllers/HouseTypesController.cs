using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.Security;

namespace Bnbooking.API.Controllers
{
    [Route("api/[controller]")]
     public class HouseTypesController : BaseController 
     {
        private const string FAILGETENTITIES = "Failed to get HouseTypes from the API";
        private const string FAILGETENTITYBYID = "Failed to get HouseTypes from the API by Id: {0}";

        public HouseTypesController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "Get HouseTypes")]
        public async Task<IActionResult> GetHouseTypes([FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.HouseTypes.ToListAsync():await ApplicationDbContext.HouseTypes.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{HouseTypeId:int}", Name = "GetHouseTypeById")]
        public async Task<IActionResult> GetHouseTypeById(Int16 HouseTypeId, [FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.HouseTypes.FirstOrDefaultAsync(o => o.Id == HouseTypeId):await ApplicationDbContext.HouseTypes.FirstOrDefaultAsync(o => o.Id == HouseTypeId);
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }
    }
}
