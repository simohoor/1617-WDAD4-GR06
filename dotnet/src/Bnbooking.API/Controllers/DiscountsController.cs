using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.Security;

namespace Bnbooking.API.Controllers
{
    [Route("api/[controller]")]
     public class DiscountsController : BaseController 
     {
        private const string FAILGETENTITIES = "Failed to get Discounts from the API";
        private const string FAILGETENTITYBYID = "Failed to get Discount from the API by Id: {0}";

        public DiscountsController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "Get Discounts")]
        public async Task<IActionResult> GetDiscounts([FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.Discounts.ToListAsync():await ApplicationDbContext.Discounts.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{DiscountId:int}", Name = "GetDiscountById")]
        public async Task<IActionResult> GetDiscountById(Int16 DiscountId, [FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.Discounts.FirstOrDefaultAsync(o => o.Id == DiscountId):await ApplicationDbContext.Discounts.FirstOrDefaultAsync(o => o.Id == DiscountId);
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }
    }
}
