using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.Security;

namespace Bnbooking.API.Controllers
{
    [Route("api/[controller]")]
     public class CitiesController : BaseController 
     {
        private const string FAILGETENTITIES = "Failed to get citys from the API";
        private const string FAILGETENTITYBYID = "Failed to get city from the API by Id: {0}";

        public CitiesController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "Get Cities")]
        public async Task<IActionResult> GetCities([FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.Cities.ToListAsync():await ApplicationDbContext.Cities.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{cityId:int}", Name = "GetCityById")]
        public async Task<IActionResult> GetCityById(Int16 cityId, [FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.Cities.FirstOrDefaultAsync(o => o.Id == cityId):await ApplicationDbContext.Cities.FirstOrDefaultAsync(o => o.Id == cityId);
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }
    }
}
