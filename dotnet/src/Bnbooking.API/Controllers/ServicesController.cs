using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.Security;

namespace Bnbooking.API.Controllers
{
    [Route("api/[controller]")]
     public class ServicesController : BaseController 
     {
        private const string FAILGETENTITIES = "Failed to get ServicesController from the API";
        private const string FAILGETENTITYBYID = "Failed to get ServicesController from the API by Id: {0}";

        public ServicesController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "Get Services")]
        public async Task<IActionResult> GetServices([FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.Services.ToListAsync():await ApplicationDbContext.Services.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{ServiceId:int}", Name = "GetServiceById")]
        public async Task<IActionResult> GetServiceById(Int16 ServiceId, [FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.Services.FirstOrDefaultAsync(o => o.Id == ServiceId):await ApplicationDbContext.Services.FirstOrDefaultAsync(o => o.Id == ServiceId);
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }
    }
}
