using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.Security;

namespace Bnbooking.API.Controllers
{
    [Route("api/[controller]")]
     public class NotRentablesController : BaseController 
     {
        private const string FAILGETENTITIES = "Failed to get NotRentables from the API";
        private const string FAILGETENTITYBYID = "Failed to get NotRentable from the API by Id: {0}";

        public NotRentablesController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "Get NotRentables")]
        public async Task<IActionResult> GetNotRentables([FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.NotRentables.ToListAsync():await ApplicationDbContext.NotRentables.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{NotRentableId:int}", Name = "GetNotRentableById")]
        public async Task<IActionResult> GetNotRentableById(Int64 NotRentableId, [FromQuery] Nullable<bool> withChildren)
        {
            var model = (withChildren != null && withChildren == true)?await ApplicationDbContext.NotRentables.FirstOrDefaultAsync(o => o.Id == NotRentableId):await ApplicationDbContext.NotRentables.FirstOrDefaultAsync(o => o.Id == NotRentableId);
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }
    }
}
