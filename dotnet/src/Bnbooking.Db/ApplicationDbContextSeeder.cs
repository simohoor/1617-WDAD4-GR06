using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Bogus;
using Bogus.DataSets;
using Bogus.Extensions;
using Bnbooking.Models;
using Bnbooking.Models.Security;

namespace Bnbooking.Db
{
    public class ApplicationDbContextSeeder 
    {
        public static async void Initialize(IServiceProvider serviceProvider)
        {
            var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();
            using(var context = serviceProvider.GetService<ApplicationDbContext>()) 
            {
                // Random instance
                var random = new Random();
                
                // Cities
                if (!context.Cities.Any())
                {
                    context.Cities.AddRange(new List<City>()
                    {
                        new City { Name = "Barcelona", Description = "Barcelona ligt in Catalonië, Spanje", PostalCode = "08003", Longtitude = 2.154007, Latitude = 41.390205, Country = "Spanje", ImageUrl = "http://static2.vtm.vmmacdn.be/sites/vtm.be/files/person/image/2016/10/europska-putovanja-barcelona-4.jpg" },
                        new City { Name = "Londen", Description = "Londen ligt in Engeland, Groot-Brittanië", PostalCode = "WC2N", Longtitude = -0.126236, Latitude = 51.500152, Country = "Engeland", ImageUrl = "http://static.pexels.com/photos/19972/pexels-photo.jpg" },
                        new City { Name = "Parijs", Description = "Parijs ligt in Frankrijk", PostalCode = "75008", Longtitude = 2.349014, Latitude = 48.864716, Country = "Frankrijk", ImageUrl = "http://static.pexels.com/photos/28826/pexels-photo-28826.jpg" },
                        new City { Name = "Brussel", Description = "Brussel ligt in België", PostalCode = "1000", Longtitude = 4.3517, Latitude = 50.8503, Country = "België", ImageUrl = "http://media-cdn.tripadvisor.com/media/photo-s/06/d8/be/db/paris-ou-bruxelles.jpg" },
                        new City { Name = "Berlijn", Description = "Berlijn ligt in Duitsland", PostalCode = "10117", Longtitude = 13.4105300, Latitude = 52.5243700, Country = "Duitsland", ImageUrl = "http://www.zapphyre.com/wp-content/uploads/2016/03/Berlin2.jpg" },
                        new City { Name = "Madrid", Description = "Madrid ligt in Spanje", PostalCode = "28000", Longtitude = -3.703790, Latitude = 40.416775, Country = "Spanje", ImageUrl = "http://madrid.bigdataweek.com/wp-content/uploads/sites/9/2015/09/madrid.jpg" },
                        new City { Name = "Rome", Description = "Rome ligt in Italië", PostalCode = "00100", Longtitude = 12.492373, Latitude = 41.890251, Country = "Italië", ImageUrl = "http://i3.mirror.co.uk/incoming/article7752816.ece/ALTERNATES/s615b/Rome-Saint-Peters-Square.jpg" },
                        new City { Name = "Amsterdam", Description = "Amsterdam ligt in Nederland", PostalCode = "1000", Longtitude = 4.8896900, Latitude = 52.3740300, Country = "Nederland", ImageUrl = "http://www.iamsterdam.com/media/architecture-amsterdam/g-a-muntplein-nc.jpg" },
                        new City { Name = "Praag", Description = "Praag ligt in Tsjechië", PostalCode = "10000", Longtitude = 14.4378, Latitude = 50.075538, Country = "Tsjechië", ImageUrl = "http://www.airstop.be/library/photos/11166363_praag.jpg" },
                        new City { Name = "Boedapest", Description = "Boedapest ligt in Hongarije", PostalCode = "1051", Longtitude = 19.03991, Latitude = 47.49801, Country = "Hongarije", ImageUrl = "http://www.stedentripper.com/wp-content/uploads/2014/08/Burchtheuvel-Boedapest.jpg" },
                        new City { Name = "Lissabon", Description = "Lissabon ligt in Portugal", PostalCode = "1000-260", Longtitude = -9.142685, Latitude = 38.736946, Country = "Portugal", ImageUrl = "http://cdn1.travelbird.com/thumbnail/image/listingpage-image/434/IALKYC6U" },
                        new City { Name = "Sint-Petersburg", Description = "Sint-Petersburg ligt in Rusland", PostalCode = "190000", Longtitude = 30.2641667, Latitude = 59.8944444, Country = "Rusland", ImageUrl = "http://static2.businessinsider.com/image/55f1b6509dd7cc25008b969b-1200/st-petersburg-savior-of-spilt-blood-chruch.jpg" }
                    });
                    await context.SaveChangesAsync();
                } 

                // RentalTypes
                if (!context.RentalTypes.Any())
                {
                    context.RentalTypes.AddRange(new List<RentalType>()
                    {
                        new RentalType { Name = "Gehele woning", Description = "Gasten zullen de gehele woning huren. Inclusief kamers van eventuele schoonfamilie." },
                        new RentalType { Name = "Privé kamer", Description = "Gasten delen een aantal ruimtes, maar hebben hun eigen kamer om te slapen." },
                        new RentalType { Name = "Gedeelde kamer", Description = "Gasten hebben geen kamer voor zichzelf." }
                    });
                    await context.SaveChangesAsync();
                } 

                 // HouseTypes
                if (!context.HouseTypes.Any())
                {
                    context.HouseTypes.AddRange(new List<HouseType>()
                    {
                        new HouseType { Name = "Appartement", Description = "Een afzonderlijke woning in een groter gebouw. Dit kan een flatgebouw zijn. Maar ook portiekwoningen, galerijwoningen en zelfs herenhuizen of omgebouwde bedrijfsgebouwen of kerken kunnen appartementen bevatten" },
                        new HouseType { Name = "Huis", Description = "Een persoonlijke afzonderlijke woning. Kan een bungalow, villa, herenhuis, rijhuis,.." },
                        new HouseType { Name = "Pension", Description = "Een accommodatie met slaapplaatsen voor logiesverstrekking in overwegend een- en tweepersoonskamers tegen boeking anders dan per nacht, waar afzonderlijke maaltijden, kleine etenswaren en dranken kunnen worden verstrekt aan gasten doch niet aan passanten." },
                        new HouseType { Name = "Vuurtoren", Description = "Toren aan de kust, die lichtsignalen uitzendt." },
                        new HouseType { Name = "Iglo", Description = "Ronde hut, gebouwd van blokken vaste sneeuw." },
                        new HouseType { Name = "Hut", Description = "Een primitieve behuizing voor mens en huisdier, gemaakt van ter plaatse aanwezig materiaal: hout, plaggen, leem en dergelijke" },
                        new HouseType { Name = "Woonboot", Description = "Vaartuig dat is ingericht of verbouwd voor permanente bewoning. Soms luxe bungalow op een stalen of betonnen ponton." }
                    });
                    await context.SaveChangesAsync();
                } 

                // CancelTypes
                if (!context.CancelTypes.Any())
                {
                    context.CancelTypes.AddRange(new List<CancelType>()
                    {
                        new CancelType { Name = "Flexibel", Description = "Volledige terugbetaling, zelfs 1 dag voor aankomst, m.u.v. kosten" },
                        new CancelType { Name = "Gemiddeld", Description = "Volledige restitutie 5 dagen voor aankomst, m.u.v. kosten" },
                        new CancelType { Name = "Streng", Description = "50% terugbetaling tot 1 week voor aankomst, m.u.v. servicekosten" },
                        new CancelType { Name = "Zeer streng", Description = "50% restitutie tot 30 dagen voor aankomst, met uitzondering van servicekosten." },
                        new CancelType { Name = "Super streng", Description = "50% terugbetaling tot 60 dagen voor aankomst, exclusief servicekosten" },
                    });
                    await context.SaveChangesAsync();
                } 

                // Buildings
                if (!context.Buildings.Any())
                {
                    context.Buildings.AddRange(new List<Building>()
                    {
                        new Building { Name = "Appartementos Helios", Description = "Appartement met 50 kamers" },
                        new Building { Name = "Huisje van Peter", Description = "Huis in het gezellige Gent met 3 verhuurbare kamers" },
                        new Building { Name = "Woonboot", Description = "Woonboot met 6 kamers te verhuren" },
                    });
                    await context.SaveChangesAsync();
                } 
                
                // ExtraCosts
                if (!context.ExtraCosts.Any())
                {
                    context.ExtraCosts.AddRange(new List<ExtraCost>()
                    {
                        new ExtraCost { Name = "Ontbijt", Description = "Het verzorgen van een ontbijt", Cost = 5 },
                        new ExtraCost { Name = "Kuis", Description = "Het kuisen van de kamers", Cost = 10 },
                        new ExtraCost { Name = "Uitstappen", Description = "Een uitstap regelen", Cost = 50 },
                        new ExtraCost { Name = "Persoonlijke koelkast", Description = "Een extra koelkast op de kamer", Cost = 10 }
                    });
                    await context.SaveChangesAsync();
                }     

                // NotRentables
                if (!context.NotRentables.Any())
                {
                    context.NotRentables.AddRange(new List<NotRentable>()
                    {
                        new NotRentable { Name = "datum", Description = "datum dat de plaats niet verhuurbaar is", Date = new DateTime(2016, 1, 18) },
                        new NotRentable { Name = "datum", Description = "datum dat de plaats niet verhuurbaar is", Date = new DateTime(2015, 1, 18)  },
                        new NotRentable { Name = "datum", Description = "datum dat de plaats niet verhuurbaar is", Date = new DateTime(2014, 1, 18)  },
                        new NotRentable { Name = "datum", Description = "datum dat de plaats niet verhuurbaar is", Date = new DateTime(2013, 1, 18)  }
                    });
                    await context.SaveChangesAsync();
                }     

                // Discounts
                if (!context.Discounts.Any())
                {
                    context.Discounts.AddRange(new List<Discount>()
                    {
                        new Discount { Name = "Vroegboek korting", Description = "een kleine korting voor vroeg te boeken", Percentage = 10 },
                        new Discount { Name = "Laatste kamer", Description = "Een korting gegeven om de laatste kamer te bezetten", Percentage = 15 }
                    });
                    await context.SaveChangesAsync();
                } 

                // BedTypes
                if(!context.BedTypes.Any()) 
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var bedtypeSkeleton = new Faker<Bnbooking.Models.BedType>()
                        .RuleFor(c => c.Name, f => lorem.Word())
                        .RuleFor(c => c.Description, f => String.Join(" ", lorem.Words(1)))
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("BedType created with Bogus: {0}!", u.Name);
                        });
                    
                    var bedtypes = new List<BedType>();

                    for(var i = 0; i < 10;i++)
                    {
                        var bedtype = bedtypeSkeleton.Generate();
                        bedtypes.Add(bedtype);
                    }

                    context.BedTypes.AddRange(bedtypes);
                    await context.SaveChangesAsync();
                }

                // Places
                if (!context.Places.Any())
                {
                    context.Places.AddRange(new List<Place>()
                    {
                        new Place { Name = "Huis x Room 2", Description = "gelegen in het centrum", HouseNr = 11, Longtitude = 2.154007, Latitude = 41.390205, Capacity = 5, CityId = 1, RentalTypeId = 2, HouseTypeId = 2, CancelTypeId = 2 },
                        new Place { Name = "Appartement x Room 407", Description = "gelegen op 5km afstand van het centrum", HouseNr = 208, Longtitude = -0.126236, Latitude = 51.500152, Capacity = 2, CityId = 2, RentalTypeId = 2, HouseTypeId = 1, CancelTypeId = 3 },
                        new Place { Name = "Jeanne's Woonboot", Description = "gelegen in de zee van Frankrijk", HouseNr = 11, Longtitude = 2.654007, Latitude = 41.390205, Capacity = 3, CityId = 1, RentalTypeId = 2, HouseTypeId = 2, CancelTypeId = 2 },
                        new Place { Name = "Pito's Iglo", Description = "gelegen in het koude noorden", HouseNr = 1, Longtitude = 5.154007, Latitude = 31.390205, Capacity = 1, CityId = 1, RentalTypeId = 2, HouseTypeId = 2, CancelTypeId = 2 },
                        new Place { Name = "Kees rooftop", Description = "gelegen in het mooie amsterdam", HouseNr = 55, Longtitude = 5.154007, Latitude = 31.390205, Capacity = 6, CityId = 8, RentalTypeId = 2, HouseTypeId = 2, CancelTypeId = 2 },
                        new Place { Name = "Christophe's appartement", Description = "gelegen in Berlijn", HouseNr = 8, Longtitude = 5.154007, Latitude = 31.390205, Capacity = 1, CityId = 5, RentalTypeId = 2, HouseTypeId = 2, CancelTypeId = 2 },
                        new Place { Name = "Petter's loft", Description = "gelegen in Budapest", HouseNr = 1, Longtitude = 5.154007, Latitude = 31.390205, Capacity = 2, CityId = 10, RentalTypeId = 2, HouseTypeId = 2, CancelTypeId = 2 },
                        new Place { Name = "Piet's kot", Description = "gelegen in het mooie belgië", HouseNr = 1, Longtitude = 5.154007, Latitude = 31.390205, Capacity = 1, CityId = 4, RentalTypeId = 2, HouseTypeId = 2, CancelTypeId = 2 },
                        new Place { Name = "Ron's ravecave", Description = "gelegen in lissabon", HouseNr = 1, Longtitude = 5.154007, Latitude = 31.390205, Capacity = 1, CityId = 11, RentalTypeId = 2, HouseTypeId = 2, CancelTypeId = 2 },
                        new Place { Name = "Appartement x Room4", Description = "gelegen in het mooie madrid", HouseNr = 1, Longtitude = 5.154007, Latitude = 31.390205, Capacity = 1, CityId = 6, RentalTypeId = 2, HouseTypeId = 2, CancelTypeId = 2 },
                        new Place { Name = "Tom's cosy flat", Description = "gelegen in parijs", HouseNr = 1, Longtitude = 5.154007, Latitude = 31.390205, Capacity = 3, CityId = 3, RentalTypeId = 2, HouseTypeId = 2, CancelTypeId = 2 },
                        new Place { Name = "Koslov's appartement", Description = "gelegen in praag", HouseNr = 1, Longtitude = 5.154007, Latitude = 31.390205, Capacity = 1, CityId = 9, RentalTypeId = 2, HouseTypeId = 2, CancelTypeId = 2 },
                        new Place { Name = "Johannes house", Description = "gelegen in Rome", HouseNr = 1, Longtitude = 5.154007, Latitude = 31.390205, Capacity = 1, CityId = 7, RentalTypeId = 2, HouseTypeId = 2, CancelTypeId = 2 },
                        new Place { Name = "Huis x room 5", Description = "gelegen in het koude Rusland", HouseNr = 1, Longtitude = 5.154007, Latitude = 31.390205, Capacity = 1, CityId = 12, RentalTypeId = 2, HouseTypeId = 2, CancelTypeId = 2 },
                        
                    });
                    await context.SaveChangesAsync();
                }

                // Prices
                if (!context.Prices.Any())
                {
                    context.Prices.AddRange(new List<Price>()
                    {
                        // Id moet hier meegegeven worden anders begint de telling niet bij 1
                        new Price { Id = 1, Name = "prijs1", Description = "Bedrag te betalen", Amount = 500, DiscountId = 1, PlaceId = 1 },
                        new Price { Id = 2, Name = "prijs2", Description = "Bedrag te betalen", Amount = 648, DiscountId = 2, PlaceId = 2 },
                        new Price { Id = 3, Name = "prijs3", Description = "Bedrag te betalen", Amount = 5400, DiscountId = 2, PlaceId = 3 },
                        new Price { Id = 4, Name = "prijs4", Description = "Bedrag te betalen", Amount = 683, DiscountId = 1, PlaceId = 4 },
                    });
                    await context.SaveChangesAsync();
                }    

                // Services
                if(!context.Services.Any()) 
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var serviceSkeleton = new Faker<Bnbooking.Models.Service>()
                        .RuleFor(c => c.Name, f => lorem.Word())
                        .RuleFor(c => c.Description, f => String.Join(" ", lorem.Words(5)))
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("Service created with Bogus: {0}!", u.Name);
                        });
                    
                    var services = new List<Service>();

                    for(var i = 0; i < 10;i++)
                    {
                        var service = serviceSkeleton.Generate();
                        services.Add(service);
                    }

                    context.Services.AddRange(services);
                    await context.SaveChangesAsync();
                } 

                // Placeservices
                if (!context.PlaceServices.Any())
                {
                    context.PlaceServices.AddRange(new List<PlaceService>()
                    {
                        new PlaceService { PlaceId = 1, ServiceId = 1 },
                        new PlaceService { PlaceId = 1, ServiceId = 2 },
                        new PlaceService { PlaceId = 1, ServiceId = 3 },
                        new PlaceService { PlaceId = 1, ServiceId = 4 },
                        new PlaceService { PlaceId = 2, ServiceId = 3 },
                    });
                    await context.SaveChangesAsync();
                }       

                // PlaceBedType
                if (!context.PlaceBedTypes.Any())
                {
                    context.PlaceBedTypes.AddRange(new List<PlaceBedType>()
                    {
                        new PlaceBedType { PlaceId = 1, BedTypeId = 1 },
                        new PlaceBedType { PlaceId = 1, BedTypeId = 2 },
                        new PlaceBedType { PlaceId = 1, BedTypeId = 3 },
                        new PlaceBedType { PlaceId = 1, BedTypeId = 4 },
                        new PlaceBedType { PlaceId = 2, BedTypeId = 3 },
                    });
                    await context.SaveChangesAsync();
                }   

                 // PlaceNotRentable
                if (!context.PlaceNotRentables.Any())
                {
                    context.PlaceNotRentables.AddRange(new List<PlaceNotRentable>()
                    {
                        new PlaceNotRentable { PlaceId = 1, NotRentableId = 1 },
                        new PlaceNotRentable { PlaceId = 1, NotRentableId = 2 },
                        new PlaceNotRentable { PlaceId = 2, NotRentableId = 3 },
                        new PlaceNotRentable { PlaceId = 3, NotRentableId = 4 },
                        new PlaceNotRentable { PlaceId = 4, NotRentableId = 3 },
                    });
                    await context.SaveChangesAsync();
                }    

                // ExtraCostPrice
                if (!context.ExtraCostPrices.Any())
                {
                    context.ExtraCostPrices.AddRange(new List<ExtraCostPrice>()
                    {
                        new ExtraCostPrice { PriceId = 1, ExtraCostId = 1 },
                        new ExtraCostPrice { PriceId = 1, ExtraCostId = 2 },
                        new ExtraCostPrice { PriceId = 2, ExtraCostId = 3 },
                        new ExtraCostPrice { PriceId = 3, ExtraCostId = 4 },
                        new ExtraCostPrice { PriceId = 4, ExtraCostId = 3 },
                    });
                    await context.SaveChangesAsync();
                }  

                // Generate Persons
                if(!context.Persons.Any()) 
                {

                    // Persons
                    var personSkeleton = new Faker<Bnbooking.Models.Person>()
                        .RuleFor(u => u.FirstName, f => f.Name.FirstName())
                        .RuleFor(u => u.SurName, f => f.Name.LastName())
                        .RuleFor(u => u.DayOfBirth, f => GenerateDateTime(1970, 1987, 1, 13, 1, 32))
                        .RuleFor(u => u.Gender, f => f.PickRandom<GenderType>())
                        .RuleFor(u => u.MartialStatus, f => f.PickRandom<MartialStatusType>())
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("User Created! Id={0}", u.Id);
                        });
                        
                   var persons = new List<Bnbooking.Models.Person>();

                    for(var i = 0; i < 100;i++)
                    {
                        var person = personSkeleton.Generate();
                        persons.Add(person);
                    }

                    context.Persons.AddRange(persons);
                    await context.SaveChangesAsync();
                }

                // Users
                if(!userManager.Users.Any()) {

                    // Get all the persons without the children
                    var persons = await context.Persons.ToListAsync();

                    Faker f = new Faker();

                    // For each person (not children) create a corresponding User
                    foreach(var person in persons)
                    {
                        // Create Bogus skeleton for a User
                        ApplicationUser user = new ApplicationUser();
                        user.UserName = f.Internet.UserName(person.FirstName, person.SurName);
                        user.Email = f.Internet.Email(person.FirstName, person.SurName);
                        user.PlainPassword = GenerateAlwaysTheSamePassword();
                        user.PersonId = person.Id;

                        Console.WriteLine("User created with Bogus: {0}!", user.UserName);

                        await userManager.CreateAsync(user, user.PlainPassword);
                    }
                }      
            }
        }

        private static DateTime GenerateDateTime(int yFrom, int yTo, int mFrom, int mTo, int dFrom, int dTo) {
            var random = new Random();
            try
            {
                return new DateTime(random.Next(yFrom, yTo), random.Next(mFrom, mTo), random.Next(dFrom, dTo));
            }
            catch(Exception ex) {
                return GenerateDateTime(yFrom, yTo, mFrom, mTo, dFrom, dTo);
            }
        }

        private static string GenerateAlwaysTheSamePassword()
        {
            return "Rode_Biet_2016";
        }
 
        private static string GeneratePassword(int length, bool upper, bool lower, bool digits, bool signs)
        {
            var source = new List<char>();
            var random = new Random();
            var result = "";
            var i = 0;
         
            if(upper)
                 source.AddRange("ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToList());
         
            if(lower)
                 source.AddRange("abcdefghijklmnopqrstuvwxyz".ToList());
         
            if(digits)
                 source.AddRange("1234567890".ToList());
         
            if(signs)
                 source.AddRange("'&#()§!$%{}\\/.;,?:+=~[]".ToList());
         
            while(i++ < length)
                 result += source[random.Next(0, source.Count)];
         
            return result;
        }
         
        public static string GeneratePassword(int length)
        {
            return GeneratePassword(length, true, true, true, true);
        }
    }
}