﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using OpenIddict;
using Npgsql.EntityFrameworkCore.PostgreSQL;
using Bnbooking.Models;
using Bnbooking.Models.Security;

namespace Bnbooking.Db
{
    public class ApplicationDbContext : OpenIddictDbContext<ApplicationUser, ApplicationRole, Guid>
    {
        
        public DbSet<City> Cities { get; set; }
        public DbSet<Place> Places { get; set; }
        public DbSet<Discount> Discounts { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<ExtraCost> ExtraCosts { get; set; }
        public DbSet<RentalType> RentalTypes { get; set; }
        public DbSet<HouseType> HouseTypes { get; set; }
        public DbSet<CancelType> CancelTypes { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<BedType> BedTypes { get; set; }
        public DbSet<PlaceService> PlaceServices { get; set; }
        public DbSet<PlaceBedType> PlaceBedTypes { get; set; }
        public DbSet<ExtraCostPrice> ExtraCostPrices { get; set; }
        public DbSet<PlaceNotRentable> PlaceNotRentables { get; set; }
        public DbSet<NotRentable> NotRentables { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Building> Buildings { get; set; }
        public DbSet<Favorite> Favorites { get; set; }
        
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasPostgresExtension("uuid-ossp");

            base.OnModelCreating(builder);

            // Model: ApplicationUser
            builder.Entity<ApplicationUser>()
                .Ignore(u => u.PlainPassword);

            builder.Entity<ApplicationUser>()
                .Property(u => u.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<ApplicationUser>()
                .Property(u => u.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            builder.Entity<ApplicationUser>()
                .HasOne(u => u.Person)
                .WithOne()
                .HasPrincipalKey<ApplicationUser>(u => u.PersonId);

            // Person
            builder.Entity<Person>()
                .HasKey(p => p.Id);

            builder.Entity<Person>()
                .HasDiscriminator<string>("PersonType")
                .HasValue<Person>("person_base");

            builder.Entity<Person>()
                .Property(p => p.FirstName)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<Person>()
                .Property(p => p.SurName)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<Person>()
                .Property(p => p.Gender)
                .IsRequired();

            builder.Entity<Person>()
                .Property(p => p.MartialStatus)
                .IsRequired(false);

            builder.Entity<Person>()
                .Property(p => p.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Person>()
                .Property(p => p.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();    

            // Model: City
            builder.Entity<City>()
                .HasKey(c => c.Id);

            builder.Entity<City>()
                .Property(c => c.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<City>()
                .Property(c => c.Description)
                .HasMaxLength(511)
                .IsRequired();

            builder.Entity<City>()
                .Property(c => c.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<City>()
                .Property(c => c.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // Model: Place
            builder.Entity<Place>()
                .HasKey(a => a.Id);

            builder.Entity<Place>()
                .Property(a => a.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<Place>()
                .Property(a => a.Description)
                .HasMaxLength(511)
                .IsRequired();

            builder.Entity<Place>()
                .Property(a => a.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Place>()
                .Property(a => a.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();    

            // Relationship between city and place: 0 to many (n)
            builder.Entity<Place>()
                .HasOne(a => a.City)
                .WithMany(c => c.Places)
                .HasForeignKey(a => a.CityId);

           // Model: RentalType
            builder.Entity<RentalType>()
                .HasKey(r => r.Id);

            builder.Entity<RentalType>()
                .Property(r => r.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<RentalType>()
                .Property(r => r.Description)
                .HasMaxLength(511)
                .IsRequired();

            builder.Entity<RentalType>()
                .Property(r => r.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<RentalType>()
                .Property(r => r.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();
                
            // Relationship between RentalType and place: 0 to many (n)
            builder.Entity<Place>()
                .HasOne(a => a.RentalType)
                .WithMany(r => r.Places)
                .HasForeignKey(a => a.RentalTypeId);  

            // Model: HouseType
            builder.Entity<HouseType>()
                .HasKey(h => h.Id);

            builder.Entity<HouseType>()
                .Property(h => h.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<HouseType>()
                .Property(h => h.Description)
                .HasMaxLength(511)
                .IsRequired();

            builder.Entity<HouseType>()
                .Property(h => h.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<HouseType>()
                .Property(h => h.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();
                
            // Relationship between HouseType and place: 0 to many (n)
            builder.Entity<Place>()
                .HasOne(a => a.HouseType)
                .WithMany(h => h.Places)
                .HasForeignKey(a => a.HouseTypeId);   

            // Model: CancelType
            builder.Entity<CancelType>()
                .HasKey(c => c.Id);

            builder.Entity<CancelType>()
                .Property(c => c.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<CancelType>()
                .Property(c => c.Description)
                .HasMaxLength(511)
                .IsRequired();

            builder.Entity<CancelType>()
                .Property(c => c.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<CancelType>()
                .Property(c => c.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();
                
            // Relationship between CancelType and place: 0 to many (n)
            builder.Entity<Place>()
                .HasOne(a => a.CancelType)
                .WithMany(c => c.Places)
                .HasForeignKey(a => a.CancelTypeId);                   
            
            // Model: Discount
            builder.Entity<Discount>()
                .HasKey(d => d.Id);

            builder.Entity<Discount>()
                .Property(d => d.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<Discount>()
                .Property(d => d.Description)
                .HasMaxLength(511)
                .IsRequired();

            builder.Entity<Discount>()
                .Property(d => d.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Discount>()
                .Property(d => d.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();  

            // Model: Price
            builder.Entity<Price>()
                .HasKey(p => p.Id);

            builder.Entity<Price>()
                .Property(p => p.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<Price>()
                .Property(p => p.Description)
                .HasMaxLength(511)
                .IsRequired();

            builder.Entity<Price>()
                .Property(p => p.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Price>()
                .Property(p => p.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // Relationship between Discount and Price: 0 to many (n)
            builder.Entity<Price>()
                .HasOne(q => q.Discount)
                .WithMany(d => d.Prices)
                .HasForeignKey(q => q.DiscountId);   
        
            
            // Relationship between place and price 1:1
            builder.Entity<Place>()
                .HasOne(a => a.Price)
                .WithOne(p => p.Place)
                .HasForeignKey<Place>(a => a.PriceId); 

            //Relationship between Price and place 1:1
            builder.Entity<Price>()
                .HasOne(a => a.Place)
                .WithOne(p => p.Price)
                .HasForeignKey<Price>(a => a.PlaceId);    
 

            // Model: ExtraCost
            builder.Entity<ExtraCost>()
                .HasKey(e => e.Id);

            builder.Entity<ExtraCost>()
                .Property(e => e.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<ExtraCost>()
                .Property(e => e.Description)
                .HasMaxLength(511)
                .IsRequired();

            builder.Entity<ExtraCost>()
                .Property(e => e.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<ExtraCost>()
                .Property(e => e.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // Many-To-Many in EF7 (10-10-2016)
            // Price can have many ExtraCosts
            // ExtraCost can be assigned to many Prices
            builder.Entity<ExtraCostPrice>()
                .HasKey(t => new { t.PriceId, t.ExtraCostId });

            builder.Entity<ExtraCostPrice>()
                .HasOne(jt => jt.Price)
                .WithMany(j => j.ExtraCosts)
                .HasForeignKey(jt => jt.PriceId);

            builder.Entity<ExtraCostPrice>()
                .HasOne(jt => jt.ExtraCost)
                .WithMany(j => j.Prices)
                .HasForeignKey(jt => jt.ExtraCostId); 

            // Model: NotRentable
            builder.Entity<NotRentable>()
                .HasKey(n => n.Id);

            builder.Entity<NotRentable>()
                .Property(n => n.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<NotRentable>()
                .Property(n => n.Description)
                .HasMaxLength(511)
                .IsRequired();

            builder.Entity<NotRentable>()
                .Property(n => n.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<NotRentable>()
                .Property(n => n.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // Many-To-Many in EF7 (10-10-2016)
            // Place can have many NotRentables
            // NotRentable can be assigned to many Places
            builder.Entity<PlaceNotRentable>()
                .HasKey(t => new { t.PlaceId, t.NotRentableId });

            builder.Entity<PlaceNotRentable>()
                .HasOne(jt => jt.Place)
                .WithMany(j => j.NotRentables)
                .HasForeignKey(jt => jt.PlaceId);

            builder.Entity<PlaceNotRentable>()
                .HasOne(jt => jt.NotRentable)
                .WithMany(j => j.Places)
                .HasForeignKey(jt => jt.NotRentableId);         

            // Model: Service
            builder.Entity<Service>()
                .HasKey(s => s.Id);

            builder.Entity<Service>()
                .Property(s => s.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<Service>()
                .Property(s => s.Description)
                .HasMaxLength(511)
                .IsRequired();

            builder.Entity<Service>()
                .Property(s => s.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Service>()
                .Property(s => s.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();    

            // Many-To-Many in EF7 (10-10-2016)
            // Place can have many Services
            // Service can be assigned to many Places
            builder.Entity<PlaceService>()
                .HasKey(t => new { t.PlaceId, t.ServiceId });

            builder.Entity<PlaceService>()
                .HasOne(pt => pt.Place)
                .WithMany(p => p.Services)
                .HasForeignKey(pt => pt.PlaceId);

            builder.Entity<PlaceService>()
                .HasOne(pt => pt.Service)
                .WithMany(p => p.Places)
                .HasForeignKey(pt => pt.ServiceId);

            // Model: BedType
            builder.Entity<BedType>()
                .HasKey(b => b.Id);

            builder.Entity<BedType>()
                .Property(b => b.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<BedType>()
                .Property(b => b.Description)
                .HasMaxLength(511)
                .IsRequired();

            builder.Entity<BedType>()
                .Property(b => b.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<BedType>()
                .Property(b => b.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();    

            // Many-To-Many in EF7 (10-10-2016)
            // Place can have many BedTypes
            // BedTypes can be assigned to many Places
            builder.Entity<PlaceBedType>()
                .HasKey(t => new { t.PlaceId, t.BedTypeId });

            builder.Entity<PlaceBedType>()
                .HasOne(pt => pt.Place)
                .WithMany(p => p.BedTypes)
                .HasForeignKey(pt => pt.PlaceId);

            builder.Entity<PlaceBedType>()
                .HasOne(pt => pt.BedType)
                .WithMany(p => p.Places)
                .HasForeignKey(pt => pt.BedTypeId);  

            // Model: Building
            builder.Entity<Building>()
                .HasKey(c => c.Id);

            builder.Entity<Building>()
                .Property(c => c.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<Building>()
                .Property(c => c.Description)
                .HasMaxLength(511)
                .IsRequired();

            builder.Entity<Building>()
                .Property(c => c.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Building>()
                .Property(c => c.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // Model: Favorite
            builder.Entity<Favorite>()
                .HasKey(c => c.Id);

            builder.Entity<Favorite>()
                .Property(c => c.Name)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<Favorite>()
                .Property(c => c.Description)
                .HasMaxLength(511)
                .IsRequired();

            builder.Entity<Favorite>()
                .Property(c => c.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Favorite>()
                .Property(c => c.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();          
        }
    }
}
