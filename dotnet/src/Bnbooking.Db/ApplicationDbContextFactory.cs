using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Npgsql.EntityFrameworkCore.PostgreSQL;
using Bnbooking.Models;
using Bnbooking.Models.Security;

namespace Bnbooking.Db
{
    public class ApplicationDbContextFactory : IDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext Create(DbContextFactoryOptions options)
        {
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UseNpgsql("User ID=postgres;Password=bnbooking;Host=localhost;Port=5432;Database=bnbooking;Pooling=true;");
            return new ApplicationDbContext(builder.Options);
        }
    }
}