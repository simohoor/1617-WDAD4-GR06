using System;
using System.Collections.Generic;

namespace Bnbooking.Models
{
    public class NotRentable : BaseEntity<Int64>
    {
        public DateTime Date { get; set; }
        public List<PlaceNotRentable> Places { get; set; }
    }
}
