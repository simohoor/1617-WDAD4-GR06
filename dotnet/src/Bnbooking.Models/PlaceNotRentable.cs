using System;
using System.Collections.Generic;

namespace Bnbooking.Models
{
    public class PlaceNotRentable
    {
       public Int32 PlaceId { get; set; }
       public Place Place { get; set; }

       public Int64 NotRentableId { get; set; }
       public NotRentable NotRentable { get; set; }
    }
}
