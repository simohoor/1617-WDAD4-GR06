using System;
using System.Collections.Generic;

namespace Bnbooking.Models
{
    public class ExtraCostPrice
    {
       public Int32 PriceId { get; set; }
       public Price Price { get; set; }

       public Int32 ExtraCostId { get; set; }
       public ExtraCost ExtraCost { get; set; }
    
    }
}
