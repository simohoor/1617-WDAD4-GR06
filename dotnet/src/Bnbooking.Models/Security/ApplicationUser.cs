using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using OpenIddict;
using Bnbooking.Models;

namespace Bnbooking.Models.Security
{
    public class ApplicationUser : IdentityUser<Guid> 
    {
        /* 
        Do not Map this field it's only usefull during the creation of a user by Bogus
        Fluent API: modelBuilder.Entity<ApplicationUser>().Ignore(u => u.PlainPassword);
        */
        public string PlainPassword  { get; set; }
 
        public Nullable<Int64> PersonId { get; set; }
        public Person Person { get; set; }
        
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }
    }
}