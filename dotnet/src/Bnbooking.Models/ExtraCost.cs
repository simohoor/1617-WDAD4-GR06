using System;
using System.Collections.Generic;

namespace Bnbooking.Models
{
    public class ExtraCost : BaseEntity<Int32>
    {
        public double Cost { get; set; }

        public List<ExtraCostPrice> Prices { get; set; }
    
    }
}
