using System;
using System.Collections.Generic;

namespace Bnbooking.Models
{
    public class City : BaseEntity<Int16>
    {
        public string PostalCode { get; set; }
        public double Longtitude { get; set; }
        public double Latitude { get; set; }
        public string Country { get; set; }
        public string ImageUrl { get; set; }
        public List<Place> Places { get; set; }
    
    }
}
