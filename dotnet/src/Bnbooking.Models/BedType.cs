using System;
using System.Collections.Generic;

namespace Bnbooking.Models
{
    public class BedType : BaseEntity<Int64>
    {
        public List<PlaceBedType> Places { get; set; }
    }
}
