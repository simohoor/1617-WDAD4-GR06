using System;
using System.Collections.Generic;

namespace Bnbooking.Models
{
    public class Price : BaseEntity<Int32>
    {
        public int Amount { get; set; }

        public Discount Discount { get; set; }
        public Int32 DiscountId { get; set; }

        public Place Place { get; set; }
        public Int32 PlaceId { get; set; }
        
        public List<ExtraCostPrice> ExtraCosts { get; set; }
    }
}
