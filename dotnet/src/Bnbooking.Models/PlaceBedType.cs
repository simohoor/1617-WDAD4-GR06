using System;
using System.Collections.Generic;

namespace Bnbooking.Models
{
    public class PlaceBedType
    {
       public Int32 PlaceId { get; set; }
       public Place Place { get; set; }

       public Int64 BedTypeId { get; set; }
       public BedType BedType { get; set; }
    }
}
