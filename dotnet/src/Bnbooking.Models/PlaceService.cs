using System;
using System.Collections.Generic;

namespace Bnbooking.Models
{
    public class PlaceService
    {
       public Int32 PlaceId { get; set; }
       public Place Place { get; set; }

       public Int64 ServiceId { get; set; }
       public Service Service { get; set; }
    }
}
