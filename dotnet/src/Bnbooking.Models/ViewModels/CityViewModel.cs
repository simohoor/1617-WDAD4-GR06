using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Bnbooking.Models;

namespace Bnbooking.Models.ViewModels
{
    public class CityViewModel
    {
        public City City { get; set; }
    }
}