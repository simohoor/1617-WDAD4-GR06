using System;
using Bnbooking.Models;

namespace Bnbooking.Models.ViewModels
{
    public class ActionPersonViewModel : ActionViewModel
    {
        public Int64 Id { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
    }
}