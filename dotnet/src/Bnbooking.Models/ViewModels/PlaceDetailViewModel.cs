using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Bnbooking.Models;

namespace Bnbooking.Models.ViewModels
{
    public class PlaceDetailViewModel
    {
        public Place Place { get; set; }
        public List<SelectListItem> Cities { get; set; }
        public List<SelectListItem> HouseTypes { get; set; }
        public MultiSelectList BedTypes { get; set; }
        public List<SelectListItem> RentalTypes { get; set; }
        public List<SelectListItem> CancelTypes { get; set; }
    }
}