using System;
using Bnbooking.Models;

namespace Bnbooking.Models.ViewModels
{
    public class ActionDiscountViewModel : ActionBaseEntityViewModel<Int32>
    {
        public int Percentage { get; set; }
    }
}