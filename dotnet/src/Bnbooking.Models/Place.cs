using System;
using System.Collections.Generic;

namespace Bnbooking.Models
{
    public class Place : BaseEntity<Int32>
    {
        public Int32 HouseNr { get; set; }
        public double Longtitude { get; set; }
        public double Latitude { get; set; }
        public Int32 Capacity { get; set; }

        public City City { get; set; }
        public Int16 CityId { get; set; }
     
        public Price Price { get; set; }
        public Int32 PriceId { get; set; }

        public RentalType RentalType { get; set; }
        public Int16 RentalTypeId { get; set; }
        
        public HouseType HouseType { get; set; }
        public Int16 HouseTypeId { get; set; }

        public CancelType CancelType { get; set; }
        public Int32 CancelTypeId { get; set; }

        public List<PlaceService> Services { get; set; }
        public List<PlaceBedType> BedTypes { get; set; }
        public List<PlaceNotRentable> NotRentables { get; set; }
    }
}
