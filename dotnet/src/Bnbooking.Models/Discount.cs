using System;
using System.Collections.Generic;

namespace Bnbooking.Models
{
    public class Discount : BaseEntity<Int32>
    {
        public int Percentage { get; set; }
        public List<Price> Prices { get; set; }    
    }
}
