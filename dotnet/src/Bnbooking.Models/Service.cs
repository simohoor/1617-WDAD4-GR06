using System;
using System.Collections.Generic;

namespace Bnbooking.Models
{
    public class Service : BaseEntity<Int64>
    {
        public List<PlaceService> Places { get; set; }
    }
}
