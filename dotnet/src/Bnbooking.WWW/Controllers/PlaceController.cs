using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.Security;
using Bnbooking.Models.Utilities;
using Bnbooking.Models.ViewModels;

namespace Bnbooking.Controllers 
{

    public class PlaceController : BaseController
    {
        public PlaceController(ApplicationDbContext applicationDbContext):base(applicationDbContext) 
        {
        }

        [HttpGet]
        public async Task<IActionResult> Index(Int32? id) 
        {
            var model = await ApplicationDbContext.Places.OrderBy(o => o.Name).Include(c => c.City).ToListAsync();      
            if (id == null)
            {               
               return View(model); 
            }   else
            {      
                           
                model = await ApplicationDbContext.Places.OrderBy(o => o.Name).Where(p => p.CityId == id).Include(c => c.City).ToListAsync();  
                ViewBag.title = "id"; 
                return View(model);
            }

            
        }
        
        [HttpGet]
        public async Task<IActionResult> Detail(Int32? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            
            var model = await ApplicationDbContext.Places.FirstOrDefaultAsync(m => m.Id == id);
            
            if(model == null)
            {
                return RedirectToAction("Index");
            }

            var viewModel = await ViewModel(model);
            
            return View(viewModel);
        }
        private async Task<PlaceDetailViewModel> ViewModel(Place Place = null) 
        {
            var Cities = await ApplicationDbContext.Cities.Select(o => new SelectListItem { 
                Value = o.Id.ToString(), 
                Text = o.Name 
            }).ToListAsync();
            var HouseTypes = await ApplicationDbContext.HouseTypes.Select(o => new SelectListItem { 
                Value = o.Id.ToString(), 
                Text = o.Name 
            }).ToListAsync();
            var RentalTypes = await ApplicationDbContext.RentalTypes.Select(o => new SelectListItem { 
                Value = o.Id.ToString(), 
                Text = o.Name 
            }).ToListAsync();
             var CancelTypes = await ApplicationDbContext.CancelTypes.Select(o => new SelectListItem { 
                Value = o.Id.ToString(), 
                Text = o.Name 
            }).ToListAsync();
            var BedTypes = await ApplicationDbContext.BedTypes.Select(o => new SelectListItem { 
                Value = o.Id.ToString(), 
                Text = o.Name 
            }).ToListAsync();

            
            var bedTypes = await ApplicationDbContext.BedTypes.ToListAsync();
            var viewModel = new PlaceDetailViewModel 
            {
                Place = (Place != null)?Place:new Place(),
                Cities = Cities,
                HouseTypes = HouseTypes,
                BedTypes = new MultiSelectList(bedTypes, "Id", "Name"),
                RentalTypes = RentalTypes,
                CancelTypes = CancelTypes
            };

            return viewModel;
        }

    }

}