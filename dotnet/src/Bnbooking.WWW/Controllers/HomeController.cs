using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.Security;
using Bnbooking.Models.Utilities;
namespace Bnbooking.Controllers 
{
    public class HomeController : BaseController
    {
        public HomeController(ApplicationDbContext applicationDbContext):base(applicationDbContext) 
        {
        }
       
        public async Task<IActionResult> Index()
        {
            var model = await ApplicationDbContext.Cities.OrderBy(o => o.Name).ToListAsync();         

            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            
            return View("Contact");
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}