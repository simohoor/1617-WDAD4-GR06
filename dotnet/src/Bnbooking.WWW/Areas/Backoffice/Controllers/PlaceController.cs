using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.Security;
using Bnbooking.Models.Utilities;
using Bnbooking.Models.ViewModels;

namespace  Bnbooking.WWW.Areas.Backoffice.Controllers 
{
    [Area("Backoffice")]
    public class PlaceController : BaseController 
    {
        public PlaceController(ApplicationDbContext applicationDbContext):base(applicationDbContext) 
        {
        }

        public async Task<IActionResult> Index(string sortParam, string searchString) 
        {
            var model = await ApplicationDbContext.Places.OrderBy(o => o.Name).Include(c => c.City).ToListAsync();

            if(!String.IsNullOrEmpty(searchString))
            {
                model = await ApplicationDbContext.Places.Where(o => o.Name.Contains(searchString)).OrderByDescending(o => o.CreatedAt).ToListAsync();
            }
            else
            {
                model = await ApplicationDbContext.Places.OrderByDescending(o => o.CreatedAt).ToListAsync();
            }
            
            if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest") 
            {
                return PartialView("_ListPartial", model);
            }
            
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Create() 
        {  
            var viewModel = await ViewModel();
            
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PlaceViewModel model)
        {
            var alert = new Alert();
            try
            {
                if(!ModelState.IsValid) {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.INVALID;
                    throw new Exception();
                }
                
                ApplicationDbContext.Places.Add(model.Place);
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.CREATENOK;
                    throw new Exception();
                }   

                alert.Message = ApplicationDbContextMessage.CREATEOK;
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;

                model = await ViewModel(model.Place);

                ModelState.AddModelError(string.Empty, alert.ExceptionMessage);
            }
            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            
            var model = await ApplicationDbContext.Places.FirstOrDefaultAsync(m => m.Id == id);
            
            if(model == null)
            {
                return RedirectToAction("Index");
            }

            var viewModel = await ViewModel(model);
            
            return View(viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(PlaceViewModel model)
        {
            var alert = new Alert();
            try 
            {
                if(!ModelState.IsValid)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.INVALID;
                    throw new Exception();
                }   
                    
                var originalModel = ApplicationDbContext.Places.FirstOrDefault(m => m.Id == model.Place.Id);
                
                if(originalModel == null) 
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }
                    
                originalModel.Name = model.Place.Name;
                originalModel.Description = model.Place.Description;
                originalModel.CityId = model.Place.CityId;
                originalModel.BedTypes = model.Place.BedTypes;
                originalModel.HouseTypeId = model.Place.HouseTypeId;
                originalModel.RentalTypeId = model.Place.RentalTypeId;
                originalModel.CancelTypeId = model.Place.CancelTypeId;
                
                ApplicationDbContext.Places.Attach(originalModel);
                ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.EDITNOK;
                    throw new Exception();
                } 
                
                alert.Message = ApplicationDbContextMessage.EDITOK;
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;

                model = await ViewModel(model.Place);

                ModelState.AddModelError(string.Empty, alert.ExceptionMessage);
            }
            return View(model);
        }

        [HttpGet("[area]/[controller]/[action]/{id:int}")]
        public async Task<IActionResult> Delete(Int16 id, [FromQuery] ActionType actionType)
        {
            var model = await ApplicationDbContext.Places.FirstOrDefaultAsync(m => m.Id == id);
            
            if(model == null)
            {
                return RedirectToAction("Index");
            }
            
            var viewModel = new ActionPlaceViewModel()
            {
                BaseEntity = model,
                ActionType = actionType
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ActionPlaceViewModel model)
        {
            var alert = new Alert();// Alert
            try
            {
                var originalModel = ApplicationDbContext.Places.FirstOrDefault(m => m.Id == model.BaseEntity.Id);
                
                if(originalModel == null)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }

                switch(model.ActionType)
                {
                    case ActionType.Delete:
                        alert.Message = ApplicationDbContextMessage.DELETEOK;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Deleted;
                        break;
                    case ActionType.SoftDelete:
                        alert.Message = ApplicationDbContextMessage.SOFTDELETEOK;
                        originalModel.DeletedAt = DateTime.Now;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                        break;
                    case ActionType.SoftUnDelete:
                        alert.Message = ApplicationDbContextMessage.SOFTUNDELETEOK;
                        originalModel.DeletedAt = (Nullable<DateTime>)null;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                        break;
                }

                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {                   
                    switch(model.ActionType)
                    {
                        case ActionType.Delete:
                            alert.Message = ApplicationDbContextMessage.DELETENOK;
                            break;
                        case ActionType.SoftDelete:
                            alert.Message = ApplicationDbContextMessage.SOFTDELETENOK;
                            break;
                        case ActionType.SoftUnDelete:
                            alert.Message = ApplicationDbContextMessage.SOFTUNDELETENOK;
                            break;
                    }
                    throw new Exception();
                } 

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }

        private async Task<PlaceViewModel> ViewModel(Place Place = null) 
        {
            var Cities = await ApplicationDbContext.Cities.Select(o => new SelectListItem { 
                Value = o.Id.ToString(), 
                Text = o.Name 
            }).ToListAsync();
            var HouseTypes = await ApplicationDbContext.HouseTypes.Select(o => new SelectListItem { 
                Value = o.Id.ToString(), 
                Text = o.Name 
            }).ToListAsync();
            var RentalTypes = await ApplicationDbContext.RentalTypes.Select(o => new SelectListItem { 
                Value = o.Id.ToString(), 
                Text = o.Name 
            }).ToListAsync();
             var CancelTypes = await ApplicationDbContext.CancelTypes.Select(o => new SelectListItem { 
                Value = o.Id.ToString(), 
                Text = o.Name 
            }).ToListAsync();
            var BedTypes = await ApplicationDbContext.BedTypes.Select(o => new SelectListItem { 
                Value = o.Id.ToString(), 
                Text = o.Name 
            }).ToListAsync();

            
            var bedTypes = await ApplicationDbContext.BedTypes.ToListAsync();
            var viewModel = new PlaceViewModel 
            {
                Place = (Place != null)?Place:new Place(),
                Cities = Cities,
                HouseTypes = HouseTypes,
                BedTypes = new MultiSelectList(bedTypes, "Id", "Name"),
                RentalTypes = RentalTypes,
                CancelTypes = CancelTypes
            };

            return viewModel;
        }
  
    }
}