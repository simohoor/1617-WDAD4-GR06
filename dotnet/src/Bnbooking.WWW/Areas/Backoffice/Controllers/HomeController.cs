using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.Security;

namespace  Bnbooking.WWW.Areas.Backoffice.Controllers 
{
    [Area("Backoffice")]
    public class HomeController : BaseController 
    {
        public HomeController():base()
        {
        }

        public IActionResult Index() 
        {
            return View();
        }
    }
}