using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.Security;
using Bnbooking.Models.Utilities;
using Bnbooking.Models.ViewModels;

namespace Bnbooking.WWW.Areas.Backoffice.Controllers 
{
    [Area("Backoffice")]
    public class PersonController : BaseController 
    {
        public PersonController(ApplicationDbContext applicationDbContext):base(applicationDbContext)
        {
        }

        public async Task<IActionResult> Index(string sortParam, string searchString) 
        {
            List<Person> persons = null;

            if(!String.IsNullOrEmpty(searchString))
            {
                persons = await ApplicationDbContext.Persons.Where(o => o.FirstName.Contains(searchString) || o.SurName.Contains(searchString)).OrderByDescending(o => o.CreatedAt).ToListAsync();
            }
            else
            {
                persons = await ApplicationDbContext.Persons.OrderByDescending(o => o.CreatedAt).ToListAsync();
            }

            ViewBag.FirstNameSortParam = "firstname_asc";
            ViewBag.SurNameSortParam = "surname_asc";
            ViewBag.CreatedSortParam = "created_asc";

            if(String.IsNullOrEmpty(sortParam))
            {
                persons = persons.OrderByDescending(o => o.CreatedAt).ToList();
            }
            else
            {
                switch(sortParam)
                {
                    case "firstname_asc":
                        ViewBag.FirstNameSortParam = "firstname_desc";
                        persons = persons.OrderBy(o => o.FirstName).ToList();
                        break;
                    case "firstname_desc":
                        ViewBag.FirstNameSortParam = "firstname_asc";
                        persons = persons.OrderByDescending(o => o.FirstName).ToList();
                        break;
                    case "surname_asc":
                        ViewBag.SurNameSortParam = "surname_desc";
                        persons = persons.OrderBy(o => o.SurName).ToList();
                        break;
                    case "surname_desc":
                        ViewBag.SurNameSortParam = "surname_asc";
                        persons = persons.OrderByDescending(o => o.SurName).ToList();
                        break;
                    case "created_asc":
                        ViewBag.CreatedSortParam = "created_desc";
                        persons = persons.OrderBy(o => o.CreatedAt).ToList();
                        break;
                    case "created_desc":
                        ViewBag.CreatedSortParam = "created_asc";
                        persons = persons.OrderByDescending(o => o.CreatedAt).ToList();
                        break;
                }
            }

            if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest") 
            {
                return PartialView("_ListPartial", persons);
            }
            
            return View(persons);

        }

        [HttpGet]
        public IActionResult Create() 
        {
            var model = new Person();

            return View(model);
        } 

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Person model)
        {
            var alert = new Alert();
            try
            {
                if(!ModelState.IsValid) {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.INVALID;
                    throw new Exception();
                }
                    
                
                ApplicationDbContext.Persons.Add(model);
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.CREATENOK;
                    throw new Exception();
                }   

                alert.Message = ApplicationDbContextMessage.CREATEOK;
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;

                ModelState.AddModelError(string.Empty, alert.ExceptionMessage);
            }
            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> Edit(Int64? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            
            var model = await ApplicationDbContext.Persons.FirstOrDefaultAsync(m => m.Id == id);
            
            if(model == null)
            {
                return RedirectToAction("Index");
            }
            
            return View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Person model)
        {
            var alert = new Alert();
            try 
            {
                if(!ModelState.IsValid)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.INVALID;
                    throw new Exception();
                }   
                    
                var originalModel = ApplicationDbContext.Persons.FirstOrDefault(m => m.Id == model.Id);
                
                if(originalModel == null) 
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }
                    
                originalModel.FirstName = model.FirstName;
                originalModel.SurName = model.SurName;
                
                ApplicationDbContext.Persons.Attach(originalModel);
                ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.EDITNOK;
                    throw new Exception();
                } 
                
                alert.Message = ApplicationDbContextMessage.EDITOK;
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;

                ModelState.AddModelError(string.Empty, alert.ExceptionMessage);
            }
            return View(model);
        }

        [HttpGet("[area]/[controller]/[action]/{id:int}")]
        public async Task<IActionResult> Delete(Int64 id, [FromQuery] ActionType actionType)
        {
            var model = await ApplicationDbContext.Persons.FirstOrDefaultAsync(m => m.Id == id);
            
            if(model == null)
            {
                return RedirectToAction("Index");
            }
            
            var viewModel = new ActionPersonViewModel()
            {
                ActionType = actionType
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ActionPersonViewModel model)
        {
            var alert = new Alert();// Alert
            try
            {
                var originalModel = ApplicationDbContext.Persons.FirstOrDefault(m => m.Id == model.Id);
                
                if(originalModel == null)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }

                switch(model.ActionType)
                {
                    case ActionType.Delete:
                        alert.Message = ApplicationDbContextMessage.DELETEOK;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Deleted;
                        break;
                    case ActionType.SoftDelete:
                        alert.Message = ApplicationDbContextMessage.SOFTDELETEOK;
                        originalModel.DeletedAt = DateTime.Now;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                        break;
                    case ActionType.SoftUnDelete:
                        alert.Message = ApplicationDbContextMessage.SOFTUNDELETEOK;
                        originalModel.DeletedAt = (Nullable<DateTime>)null;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                        break;
                }

                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {                   
                    switch(model.ActionType)
                    {
                        case ActionType.Delete:
                            alert.Message = ApplicationDbContextMessage.DELETENOK;
                            break;
                        case ActionType.SoftDelete:
                            alert.Message = ApplicationDbContextMessage.SOFTDELETENOK;
                            break;
                        case ActionType.SoftUnDelete:
                            alert.Message = ApplicationDbContextMessage.SOFTUNDELETENOK;
                            break;
                    }
                    throw new Exception();
                } 

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }
    }
}