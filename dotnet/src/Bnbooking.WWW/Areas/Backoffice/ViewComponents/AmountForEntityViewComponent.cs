using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Bnbooking.Db;
using Bnbooking.Models;
using Bnbooking.Models.ViewModels;
using Bnbooking.Models.Security;

namespace  Bnbooking.WWW.Areas.Backoffice.ViewComponents 
{
    [ViewComponent(Name="AmountForEntity")]
    public class AmountForEntityViewComponent : BaseViewComponent 
    {
        public AmountForEntityViewComponent(ApplicationDbContext applicationDbContext) : base(applicationDbContext) 
        {
        }

        public async Task<IViewComponentResult> InvokeAsync(string entityType)
        {
            var viewModel = await GetAmountForEntityAsync(entityType);
            return View(viewModel);
        }

        private Task<AmountForEntityViewModel> GetAmountForEntityAsync(string entityType)
        {
            return Task.FromResult(GetAmountForEntity(entityType));
        }

        private AmountForEntityViewModel GetAmountForEntity(string entityType)
        {
            var amount = 0;
            entityType = entityType;
            var name = entityType;
            var pluralizeName = "Entities";

            switch(entityType)
            {
                case "City":
                    amount = ApplicationDbContext.Cities.AsEnumerable().Count();
                    entityType = "City";
                    name = "City";
                    pluralizeName = "Cities";
                    break;
                case "Place":
                    amount = ApplicationDbContext.Places.AsEnumerable().Count();
                    entityType = "Place";
                    name = "Place";
                    pluralizeName = "Places";
                    break;
                case "Service":
                    amount = ApplicationDbContext.Services.AsEnumerable().Count();
                    entityType = "Service";
                    name = "Service";
                    pluralizeName = "Services";
                    break;
                case "Person":
                    amount = ApplicationDbContext.Persons.AsEnumerable().Count();
                    entityType = "Person";
                    name = "User";
                    pluralizeName = "Users";
                    break;
                case "BedType":
                    amount = ApplicationDbContext.BedTypes.AsEnumerable().Count();
                    entityType = "BedType";
                    name = "BedType";
                    pluralizeName = "BedTypes";
                    break;
                case "Discount":
                    amount = ApplicationDbContext.Discounts.AsEnumerable().Count();
                    entityType = "Discount";
                    name = "Discount";
                    pluralizeName = "Discounts";
                    break;
                case "CancelType":
                    amount = ApplicationDbContext.CancelTypes.AsEnumerable().Count();
                    entityType = "CancelType";
                    name = "CancelType";
                    pluralizeName = "CancelTypes";
                    break;

            
            }

            var viewModel = new AmountForEntityViewModel()
            {
                Amount = amount,
                EntityType = entityType,
                Name = name,
                PluralizeName = pluralizeName
            };

            return viewModel;
        }
    }
}