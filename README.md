### _BnBooking_
**Lode Muylaert**

* Opleidingsonderdeel:	_Webdesign & Development 4_
* Academiejaar: _2016-2017_
* Opleiding: _Bachelor in de grafische en digitale media_
* Afstudeerrichting: _Multimediaproductie_
* Keuzeoptie: _proDEV_
* Opleidingsinstelling: _Arteveldehogeschool_
